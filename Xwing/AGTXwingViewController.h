//
//  AGTXwingViewController.h
//  Xwing
//
//  Created by Fernando Rodríguez Romero on 20/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGTXwingViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *xwingView;

@end
