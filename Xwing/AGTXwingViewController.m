//
//  AGTXwingViewController.m
//  Xwing
//
//  Created by Fernando Rodríguez Romero on 20/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTXwingViewController.h"

@interface AGTXwingViewController ()

@end

@implementation AGTXwingViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
    // Creamos un tap gesture recognizer
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userDidTap:)];
    
    // se lo encasquetamos a la vista
    [self.view addGestureRecognizer:tap];
    
}


-(void) userDidTap:(UITapGestureRecognizer*) tap{
    
    if (tap.state == UIGestureRecognizerStateRecognized) {
        
        // cambiamos la posición de la vista del xwing
        UIViewAnimationOptions option = UIViewAnimationOptionBeginFromCurrentState |UIViewAnimationOptionCurveEaseInOut;
        
        [UIView animateWithDuration:1.2
                              delay:0
                            options:option
                         animations:^{
                             
                             self.xwingView.center =
                             [tap locationInView:self.view];
                             
                         } completion:^(BOOL finished) {
                             
                         }];
        
        
        [UIView animateWithDuration:0.6
                              delay:0
                            options:option
                         animations:^{
                             self.xwingView.transform = CGAffineTransformMakeRotation(M_PI_2);
                             
                         } completion:^(BOOL finished) {
                             
                             [UIView animateWithDuration:0.6
                                                   delay:0
                                                 options:option
                                              animations:^{
                                                  self.xwingView.transform = CGAffineTransformIdentity;
                                              } completion:nil];
                             
                             
                         }];
        
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
